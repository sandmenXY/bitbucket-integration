var express = require('express');
var webpack = require('webpack');
var config = require('./webpack.config.dev');

var port = 3333;
var app = express();
var compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.use(express.static(__dirname + '/'));

app.get(/.*/, function (req, res) {
    res.sendFile(`${__dirname}/index.html`);
});

app.listen(port, function onAppListening(err) {
    if (err) {
        console.error(err);
    } else {
        console.info('==> 🚧  Webpack development server listening on port %s', port);
    }
});