import React from 'react';
import {render} from 'react-dom';
import {BrowserRouter, Route} from 'react-router-dom';
import Login from './containers/Login';
import Dashboard from './containers/Dashboard';

render(
    <BrowserRouter>
        <div className="router">
            <Route exact path="/" component={Dashboard} />
            <Route path="/login" component={Login} />
        </div>
    </BrowserRouter>,
    document.getElementById('root')
);