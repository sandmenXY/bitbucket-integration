**Create database and user**

`1. use bitbucket-integration`

`2. db.createUser({
    user: "admin",
    pwd: "admin",
    roles: [{
        role: "dbAdmin",
        db: "bitbucket-integration"
    }]
})`

`3. db.createUser({
    user: "root",
    pwd: "root",
    roles: [{
        role: "readWrite",
        db: "bitbucket-integration"
    }]
})`
