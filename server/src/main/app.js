import ApplicationConfig from './config/application.config';

const applicationConfig = new ApplicationConfig();
applicationConfig.configureApplication();
export default applicationConfig.application;