import express from 'express';
import passport from 'passport';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import ServerConfig from './server.config';
import RoutesConfig from './routes.config';
import MongooseConfig from './mongoose.config';
import PassportConfig from './passport.config';

export default class ApplicationConfig {
    constructor() {
        this.application = express();
        this.passport = passport;
    }

    configureApplication() {
        this.configureBodyParser();
        this.configureCookiesParser();
        this.configurePassport();
        this.registerRoutes();
        this.configureServer();
        ApplicationConfig.configureDatabase();
    }

    configureBodyParser() {
        this.application.use(bodyParser.urlencoded({extended: true}));
        this.application.use(bodyParser.json());
    }

    configureCookiesParser() {
        this.application.use(cookieParser());
    }

    configurePassport() {
        new PassportConfig(this.passport).configurePassport();
        this.application.use(this.passport.initialize());
    }

    registerRoutes() {
        new RoutesConfig(this.application, this.passport).registerRoutes();
    }

    configureServer() {
        new ServerConfig(this.application).configureServer();
    }

    static configureDatabase() {
        new MongooseConfig().configureDatabase();
    }
}