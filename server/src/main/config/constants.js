export const BITBUCKET_OAUTH2_CONFIGURATION = {
    provider: 'bitbucket',
    authorizationUrl: 'https://bitbucket.org/site/oauth2/authorize',
    tokenUrl: 'https://bitbucket.org/site/oauth2/access_token',
    clientId: 'RzEH5xCPcjDGerUqaj',
    clientSecret: 'L7zwWPbp5DDLhP9pRsLv7uTbLCx7RBxz',
    callbackUrl: 'http://localhost:3000/login/bitbucket/callback'
};

export const PROVIDERS = {
    tokenProvider: 'token',
    bitbucketProvider: 'bitbucket',
};

const BITBUCKET_API = {
    url: 'https://api.bitbucket.org',
    version: '2.0'
};
export const BITBUCKET_URLS = {
    userInformation: `${BITBUCKET_API.url}/${BITBUCKET_API.version}/users`,
    userRepositories: `${BITBUCKET_API.url}/${BITBUCKET_API.version}/repositories`
};

export const MAIN_ROUTE = '/';
export const ACCESS_TOKEN_PARAM = '?access_token=';

export const ROUTES = {
    login: '/login',
    loginBitbucket: '/login/bitbucket',
    dashboard: '/dashboard'
};