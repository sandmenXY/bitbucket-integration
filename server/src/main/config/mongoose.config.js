import mongoose from 'mongoose';

const MONGO_DB_CONFIG = {
    DB_CONFIG: {
        host: 'localhost',
        port: 27017,
        dbName: 'bitbucket-integration'
    },
    DB_OPTIONS: {
        db: {native_parser: true},
        server: {poolSize: 5},
        user: 'root',
        pass: 'root'
    }
};

export default class MongooseConfig {
    configureDatabase() {
        const mongoDbUri = MongooseConfig.createMongoDbUri();
        mongoose.connect(mongoDbUri, MONGO_DB_CONFIG.DB_OPTIONS);
        mongoose.connection.on('connected', () => console.log(`Mongoose default connection open to ${mongoDbUri}`));
        mongoose.connection.on('error', err => console.log(`Mongoose default connection error: ${err}`));
        mongoose.connection.on('disconnected', () => console.log('Mongoose default connection disconnected'));
    }

    static createMongoDbUri() {
        return `mongodb://${MONGO_DB_CONFIG.DB_CONFIG.host}:${MONGO_DB_CONFIG.DB_CONFIG.port}/${MONGO_DB_CONFIG.DB_CONFIG.dbName}`;
    }
}