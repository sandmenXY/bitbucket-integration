import BitbucketStrategyOptions from 'passport-bitbucket-oauth2';
import CookieStrategyOptions from 'passport-cookie';
import {BITBUCKET_OAUTH2_CONFIGURATION, PROVIDERS} from './constants';
import User from '../models/user';

export default class PassportConfig {
    constructor(passport) {
        this.passport = passport;
    }

    configurePassport() {
        this.passport.serializeUser((user, next) => next(null, user));
        this.passport.deserializeUser((obj, next) => next(null, obj));

        this.passport.use(PROVIDERS.tokenProvider, this.createLocalStrategy());
        this.passport.use(PROVIDERS.bitbucketProvider, this.createBitbucketStrategy());
    }

    createBitbucketStrategy() {
        return new BitbucketStrategyOptions.Strategy({
                clientID: BITBUCKET_OAUTH2_CONFIGURATION.clientId,
                clientSecret: BITBUCKET_OAUTH2_CONFIGURATION.clientSecret,
                callbackURL: BITBUCKET_OAUTH2_CONFIGURATION.callbackUrl
            },
            (token, refreshToken, profile, next) => {
                User.findOne({'username': profile.username}, user => {
                    if (user) return next(null, user);
                    const newUser = this.createNewUser(profile, token);
                    newUser.save(savedUser => next(null, newUser));
                });
            }
        );
    }

    createLocalStrategy() {
        return new CookieStrategyOptions.Strategy((token, next) => {
                User.findOne({'token': token}, (err, user) => {
                    if (user) return next(null, user);
                    return next(new Error("User not found"), null);
                });
            }
        );
    }

    createNewUser(profile, token) {
        const newUser = new User();
        newUser.bitbucketId = profile.id;
        newUser.displayName = profile.displayName;
        newUser.profileUrl = profile.profileUrl;
        newUser.username = profile.username;
        newUser.token = token;
        return newUser;
    };
}