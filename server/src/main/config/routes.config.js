import {routesMap} from '../routes/routes.map.js';

export default class RoutesConfig {
    constructor(application, passport) {
        this.application = application;
        this.passport = passport;
    }

    registerRoutes() {
        for (const route in routesMap) {
            this.application.use(route, routesMap[route].call(null, this.passport));
        }
    }
}