import http from 'http';
const SERVER_PORT = process.env.PORT || 3000;

export default class ServerConfig {
    constructor(application) {
        this.server = http.createServer(application);
    }

    configureServer() {
        this.server.listen(SERVER_PORT);
        this.server.on('listening', () => console.log(`Listening on ${SERVER_PORT}.`));
    }
}