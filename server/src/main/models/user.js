import mongoose from 'mongoose';
import {Schema} from 'mongoose';

const userSchema = Schema({
    bitbucketId: String,
    displayName: String,
    profileUrl: String,
    username: String,
    token : String
});

export default mongoose.model('User', userSchema);