import {Router} from 'express';
import request from 'request';
import {ACCESS_TOKEN_PARAM, BITBUCKET_URLS, MAIN_ROUTE, PROVIDERS, ROUTES} from '../config/constants';
import User from '../models/user';

export default function (passport) {
    const dashboardRouter = Router();

    dashboardRouter.route(MAIN_ROUTE)
        .get(passport.authenticate(PROVIDERS.tokenProvider, {failureRedirect: ROUTES.login}),
            function (req, response) {
                const token = req.cookies.token;
                User.findOne({'token': token}, (err, user) => {
                    request(`${BITBUCKET_URLS.userRepositories}/${user.username}/${ACCESS_TOKEN_PARAM}${user.token}`,
                        (error, bitbucketResponse, body) => response.json({response: bitbucketResponse}));
                });
            });

    return dashboardRouter;
}
