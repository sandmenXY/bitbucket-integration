import {Router} from 'express';
import {MAIN_ROUTE, PROVIDERS, ROUTES} from '../config/constants';

export default function (passport) {
    const loginRouter = Router();

    loginRouter.route(MAIN_ROUTE).get((request, response) => {
        response.redirect(!request.cookies.token ? ROUTES.loginBitbucket : ROUTES.dashboard);
    });

    loginRouter.route('/bitbucket').get(passport.authenticate(PROVIDERS.bitbucketProvider));

    loginRouter.route('/bitbucket/callback')
        .get(passport.authenticate(PROVIDERS.bitbucketProvider, {failureRedirect: ROUTES.login}),
            function (request, response) {
                response.json({user: request.user});
            });

    return loginRouter;
}