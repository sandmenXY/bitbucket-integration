import loginRoutes from './login.routes.js';
import dashboardRoutes from './dashboard.routes.js';

export const routesMap = {
    '/login': loginRoutes,
    '/dashboard': dashboardRoutes
};